const jwt = require("jsonwebtoken");
const db = require("../db/models");
const UserAdmin = db.UserAdmin;

const auth = async (req, res, next) => {
    try {
        let token = req.headers.authorization;

        if (!token) {
            return res.status(401).json({ message: "Unauthorized User2!" });
        }
        jwt.verify(
            token,
            process.env.JWT_SECRET_KEY,
            async function (err, decoded) {
                console.log(decoded);
                if (err) {
                    console.error("err", err?.message);
                    return res
                        .status(401)
                        .json({ message: "Unauthorized User2!" });
                }
                if (!decoded) {
                    return res
                        .status(401)
                        .json({ message: "Unauthorized User2!" });
                }
                // console.log(decoded..id);
                const user = await UserAdmin.findOne({
                    where: {
                        id: decoded.user.id,
                    },
                });
                if (!user) {
                    return res.status(404).send({
                        message: "User not found.",
                    });
                }
                // if (!user.status) {
                //     return res.status(400).send({
                //         message: "Contact admin.",
                //     });
                // }
                req.user = user;
                next();
            }
        );
    } catch (error) {
        console.log(error);
        return res.status(401).json({ message: "Unauthorized User2!" });
    }
};

module.exports = { auth };
