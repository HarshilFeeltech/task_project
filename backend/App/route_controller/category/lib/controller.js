const db = require("../../../db/models");

const Category = db.Category;

const createCategory = async (req, res) => {
    try {
        const { name } = req.body;

        const category = await Category.create({
            name: name,
        });
        return res.status(201).json({ data: category });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "something went wrong!" });
    }
};

const updateCategory = async (req, res) => {
    let id = req.params.id;

    const { name } = req.body;
    const update = {
        name: name,
    };

    try {
        await Category.update(update, { where: { id: id } });
        return res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
};

const deleteCategory = async (req, res) => {
    let id = req.params.id;
    const { name } = req.body;
    const update = {
        name: name,
    };

    try {
        await Category.update(update, { where: { id: id } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
    // await Role.destroy({where:{id:id,deletedby:null}})
    // res.status(200).send('DELETED')
};

const getOneCategory = async (req, res) => {
    let id = req.params.id;
    let catagory = await Category.findOne({
        where: { id: id },
    });
    return res.status(200).send(catagory);
};

const getAllCategory = async (req, res) => {
    const catagorys = await Category.findAll({});
    return res.status(200).send(catagorys);
};

module.exports = {
    createCategory,
    updateCategory,
    deleteCategory,
    getOneCategory,
    getAllCategory,
};
