// const express = require("express");
// const app = express.Router();
// const { auth } = require("../../middleware/
const {
    createVariationOption,
    updateVariationOption,
    deleteVariationOption,
    getOneVariationOption,
    getAllVariationOption,
} = require("./lib/controller");

module.exports = (app) => {
    app.get("/variationOption/allVariation", getAllVariationOption);
    app.get("/variationOption/:id", getOneVariationOption);
    app.post("/variationOption/", createVariationOption);
    app.delete("/variationOption/:id", deleteVariationOption);
    app.put("/variationOption/:id", updateVariationOption);
};
