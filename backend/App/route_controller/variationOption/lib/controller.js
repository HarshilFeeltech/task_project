const db = require("../../../db/models");

const VariationOption = db.VariationOption;

const createVariationOption = async (req, res) => {
    try {
        const { name, variationId } = req.body;

        const variationOption = await VariationOption.create({
            name: name,
            variationId: variationId,
        });
        return res.status(201).json({ data: variationOption });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "something went wrong!" });
    }
};

const updateVariationOption = async (req, res) => {
    let id = req.params.id;

    const { name, variationId } = req.body;
    const update = {
        name: name,
        variationId: variationId,
    };

    try {
        await VariationOption.update(update, { where: { id: id } });
        return res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
};

const deleteVariationOption = async (req, res) => {
    let id = req.params.id;
    const { name, variationId } = req.body;
    const update = {
        name: name,
        variationId: variationId,
    };

    try {
        await VariationOption.update(update, { where: { id: id } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
    // await Role.destroy({where:{id:id,deletedby:null}})
    // res.status(200).send('DELETED')
};

const getOneVariationOption = async (req, res) => {
    let id = req.params.id;
    let variationOption = await VariationOption.findOne({
        where: { id: id },
    });
    return res.status(200).send(variationOption);
};

const getAllVariationOption = async (req, res) => {
    const variationOption = await VariationOption.findAll({});
    return res.status(200).send(variationOption);
};

module.exports = {
    createVariationOption,
    updateVariationOption,
    deleteVariationOption,
    getOneVariationOption,
    getAllVariationOption,
};
