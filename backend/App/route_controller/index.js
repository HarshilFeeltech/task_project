module.exports = (app) => {
    require("./category")(app);
    require("./checkout")(app);
    require("./productConfig")(app);
    require("./products")(app);
    require("./productVariation")(app);
    require("./role")(app);
    require("./user")(app);
    require("./userAdmin")(app);
    require("./variation")(app);
    require("./variationOption")(app);
};
