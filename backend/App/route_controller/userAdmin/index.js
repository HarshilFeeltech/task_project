// const express = require("express");
// const app = express.Router();
const { auth } = require("../../middleware/auth");
const {
    signIn,
    addUser,
    updateUser,
    deleteUser,
    getUsers,
    getUserById,
    //     upload,
} = require("./lib/controller");

const { validation, signInvalidation } = require("./lib/validation");

module.exports = (app) => {
    app.get("/userAdmin/allUser", auth, getUsers);
    app.get("/userAdmin/:id", auth, getUserById);
    app.post("/userAdmin/", validation, auth, addUser);
    app.post("/userAdmin/signIn", signInvalidation, signIn);
    app.put("/userAdmin/:id", auth, updateUser);
    app.delete("/userAdmin/:id", auth, deleteUser);
};
