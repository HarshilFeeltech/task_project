const { check } = require("express-validator");

exports.validation = [
    check("firstName", "firstName is required").not().isEmpty(),
    check("lastName", "lastName is required").not().isEmpty(),
    check("email", "Please include a valid Email")
        .isEmail()
        .normalizeEmail({ gmail_remove_dots: true }),
    check("password", "enter valid password").isStrongPassword({
        minLength: 6,
        minLowercase: 1,
        minLowercase: 1,
        minNumbers: 1,
    }),
    check("contact", "contact is required").not().isEmpty(),
];

exports.signInvalidation = [
    check("email", "Please include a valid Email")
        .isEmail()
        .normalizeEmail({ gmail_remove_dots: true }),
    check("password", "enter valid password").isStrongPassword({
        minLength: 6,
        minLowercase: 1,
        minLowercase: 1,
        minNumbers: 1,
    }),
];
