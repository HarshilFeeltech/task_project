const db = require("../../../db/models");

const Variation = db.Variation;
const ProductVariation = db.ProductVariation;

const createProductVariation = async (req, res) => {
    try {
        const { productId, variationId } = req.body;

        const productVariation = await ProductVariation.create({
            productId: productId,
            variationId: variationId,
        });
        return res.status(201).json({ data: productVariation });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "something went wrong!" });
    }
};

const updateProductVariation = async (req, res) => {
    let id = req.params.id;

    const { productId, variationId } = req.body;
    const update = {
        productId: productId,
        variationId: variationId,
    };

    try {
        await ProductVariation.update(update, { where: { id: id } });
        return res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
};

const deleteProductVariation = async (req, res) => {
    let id = req.params.id;
    const { productId, variationId } = req.body;
    const update = {
        productId: productId,
        variationId: variationId,
    };

    try {
        await ProductVariation.update(update, { where: { id: id } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
};

const getOneProductVariation = async (req, res) => {
    let id = req.params.id;
    let catagory = await ProductConfig.findOne({
        where: { id: id },
    });
    return res.status(200).send(catagory);
};

const getAllProductVariation = async (req, res) => {
    const variations = await ProductVariation.findAll({});
    return res.status(200).send(variations);
};

module.exports = {
    createProductVariation,
    updateProductVariation,
    deleteProductVariation,
    getOneProductVariation,
    getAllProductVariation,
};
