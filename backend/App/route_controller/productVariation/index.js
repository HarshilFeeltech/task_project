// const express = require("express");
// const productVariationRouterr = express.Router();
// const { auth } = require("../../middleware/auth");
const {
    createProductVariation,
    updateProductVariation,
    deleteProductVariation,
    getOneProductVariation,
    getAllProductVariation,
} = require("../../route_controller/productVariation/lib/controller");

module.exports = (app) => {
    app.get("/productVariation/all", getAllProductVariation);
    app.get("/productVariation/:id", getOneProductVariation);
    app.post("/productVariation/", createProductVariation);
    app.delete("/productVariation/:id", deleteProductVariation);
    app.put("/productVariation/:id", updateProductVariation);
};
