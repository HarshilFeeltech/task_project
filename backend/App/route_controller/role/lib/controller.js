const { Op } = require("sequelize");
const db = require("../../../db/models");
const { validationResult } = require("express-validator");

const UserAdmin = db.UserAdmin;
const Role = db.Role;

const createRole = async (req, res) => {
    try {
        // const id = req.params.id;
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const { name, description, status } = req.body;

        const role = await Role.create({
            name: name,
            description: description,
            status: status,
            createdBy: req.user.id,
        });
        return res.status(201).json({ data: role });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "something went wrong!" });
    }
};

const updateRole = async (req, res) => {
    let id = req.params.id;

    const { name, description, status } = req.body;
    const update = {
        name: name,
        description: description,
        status: status,
        updatedBy: req.user.id,
    };

    try {
        await Role.update(update, { where: { id: id, deletedby: null } });
        return res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
};

const deleteRole = async (req, res) => {
    const currentDate = new Date();
    const timestamp = currentDate.getTime();
    let id = req.params.id;
    const { name, description, status } = req.body;
    const update = {
        name: name,
        description: description,
        status: status,
        deletedBy: req.user.id,
        deletedAt: timestamp,
    };

    try {
        await Role.update(update, { where: { id: id, deletedby: null } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
    // await Role.destroy({where:{id:id,deletedby:null}})
    // res.status(200).send('DELETED')
};

const getOneRole = async (req, res) => {
    let id = req.params.id;
    let roles = await Role.findOne({
        where: { id: id, deletedby: null },
        include: [
            {
                model: UserAdmin,
            },
        ],
    });
    return res.status(200).send(roles);
};

const getAllRole = async (req, res) => {
    const roles = await Role.findAll({
        where: {
            deletedby: null,
            //     id: { [Op.eq]: 2 },
        },
        include: [
            {
                model: UserAdmin,
            },
        ],
    });
    return res.status(200).send(roles);
};

module.exports = {
    createRole,
    updateRole,
    deleteRole,
    getOneRole,
    getAllRole,
    // findAllStatus,
};
