const { check } = require("express-validator");

exports.validation = [
    check("name", "Name is required").not().isEmpty(),
    check("description", "Description is required").not().isEmpty(),
];
