// const express = require("express");
// const app = express.Router();
const { auth } = require("../../middleware/auth");
const {
    getAllRole,
    createRole,
    deleteRole,
    updateRole,
    getOneRole,
    // findAllStatus,
} = require("../../route_controller/role/lib/controller");

module.exports = (app) => {
    app.get("/role/allRole", auth, getAllRole);
    app.get("/role/:id", auth, getOneRole);
    app.post("/role/addRole", auth, createRole);
    app.delete("/role/:id", auth, deleteRole);
    app.put("/role/:id", auth, updateRole);
};
