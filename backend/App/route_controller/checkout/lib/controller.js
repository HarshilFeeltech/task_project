const db = require("../../../db/models");

const Checkout = db.Checkout;

const createCheckout = async (req, res) => {
    try {
        // console.log(req.user.id);
        const {
            firstName,
            lastName,
            country,
            streetAddress,
            town,
            state,
            postcode,
            phone,
            email,
            orderNote,
        } = req.body;

        // console.log(req.user.id);
        const checkout = await Checkout.create({
            firstName: firstName,
            lastName: lastName,
            country: country,
            streetAddress: streetAddress,
            town: town,
            state: state,
            postcode: postcode,
            phone: phone,
            email: email,
            orderNote: orderNote,
        });
        return res.status(201).json({ data: checkout });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "something went wrong!" });
    }
};

const updateCheckout = async (req, res) => {
    let id = req.params.id;

    const {
        firstName,
        lastName,
        country,
        streetAddress,
        town,
        state,
        postcode,
        phone,
        email,
        orderNote,
    } = req.body;

    const update = {
        firstName: firstName,
        lastName: lastName,
        country: country,
        streetAddress: streetAddress,
        town: town,
        state: state,
        postcode: postcode,
        phone: phone,
        email: email,
        orderNote: orderNote,
    };

    try {
        console.log("This is Something", id);
        await Checkout.update(update, { where: { id: id } });
        return res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
};

const deleteCheckout = async (req, res) => {
    const currentDate = new Date();
    let id = req.params.id;
    const {
        firstName,
        lastName,
        country,
        streetAddress,
        town,
        state,
        postcode,
        phone,
        email,
        orderNote,
    } = req.body;

    const update = {
        firstName: firstName,
        lastName: lastName,
        country: country,
        streetAddress: streetAddress,
        town: town,
        state: state,
        postcode: postcode,
        phone: phone,
        email: email,
        orderNote: orderNote,
    };

    try {
        await Checkout.update(update, { where: { id: id, deletedby: null } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
    // await Role.destroy({where:{id:id,deletedby:null}})
    // res.status(200).send('DELETED')
};

const getOneCheckout = async (req, res) => {
    let id = req.params.id;
    let checkouts = await Checkout.findOne({
        where: { id: id, deletedby: null },
        include: [
            {
                model: User,
            },
        ],
    });
    return res.status(200).send(checkouts);
};

const getAllCheckout = async (req, res) => {
    const checkouts = await Checkout.findAll({
        where: {
            deletedby: null,
            //     id: { [Op.eq]: 2 },
        },
        include: [
            {
                model: User,
            },
        ],
    });
    return res.status(200).send(checkouts);
};

module.exports = {
    createCheckout,
    updateCheckout,
    deleteCheckout,
    getOneCheckout,
    getAllCheckout,
};
