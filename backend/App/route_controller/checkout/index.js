// const express = require("express");
// const { auth } = require("../../middleware/auth");
// const checkoutRouter = express.Router();
const {
    createCheckout,
    updateCheckout,
    deleteCheckout,
    getOneCheckout,
    getAllCheckout,
} = require("../../route_controller/checkout/lib/controller");

module.exports = (app) => {
    app.get("/checkout/allCheckout", getAllCheckout);
    app.get("/checkout/:id", getOneCheckout);
    app.post("/checkout/addCheckout", createCheckout);
    app.delete("/checkout/:id", deleteCheckout);
    app.put("/checkout/:id", updateCheckout);
};
