const db = require("../../../db/models");

const Variation = db.Variation;

const createVariation = async (req, res) => {
    try {
        const { name } = req.body;

        const variation = await Variation.create({
            name: name,
        });
        return res.status(201).json({ data: variation });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "something went wrong!" });
    }
};

const updateVariation = async (req, res) => {
    let id = req.params.id;

    const { name } = req.body;
    const update = {
        name: name,
    };

    try {
        await Variation.update(update, { where: { id: id } });
        return res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
};

const deleteVariation = async (req, res) => {
    let id = req.params.id;
    const { name } = req.body;
    const update = {
        name: name,
    };

    try {
        await Variation.update(update, { where: { id: id } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
    // await Role.destroy({where:{id:id,deletedby:null}})
    // res.status(200).send('DELETED')
};

const getOneVariation = async (req, res) => {
    let id = req.params.id;
    let variation = await Variation.findOne({
        where: { id: id },
    });
    return res.status(200).send(variation);
};

const getAllVariation = async (req, res) => {
    const variations = await Variation.findAll({});
    return res.status(200).send(variations);
};

module.exports = {
    createVariation,
    updateVariation,
    deleteVariation,
    getOneVariation,
    getAllVariation,
};
