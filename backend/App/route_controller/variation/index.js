// const express = require("express");
// const app = express.Router();
// const { auth } = require("../../middleware/auth");
const {
    createVariation,
    updateVariation,
    deleteVariation,
    getOneVariation,
    getAllVariation,
} = require("./lib/controller");

module.exports = (app) => {
    app.get("/variation/allVariation", getAllVariation);
    app.get("/variation/:id", getOneVariation);
    app.post("/variation/", createVariation);
    app.delete("/variation/:id", deleteVariation);
    app.put("/variation/:id", updateVariation);
};
