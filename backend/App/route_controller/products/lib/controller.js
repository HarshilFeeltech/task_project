const db = require("../../../db/models");
const Product = db.Product;
const ProductConfig = db.ProductConfig;
const ProductVariation = db.ProductVariation;
const VariationOption = db.VariationOption;
const Variation = db.Variation;

const multer = require("multer");
const path = require("path");

const Category = db.Category;

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "upload");
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname));
    },
});

const upload = multer({
    storage: storage,
    limits: { fileSize: "8000000" },
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/;
        const mimeType = fileTypes.test(file.mimetype);
        const extname = fileTypes.test(path.extname(file.originalname));

        if (mimeType && extname) {
            return cb(null, true);
        }
        cb("give proper file formate to upload");
    },
}).single("image");

const createProduct = async (req, res) => {
    // console.log("req", req.body);
    const variation = JSON.parse(req.body.variationId);
    const variationDetails = JSON.parse(req.body.variationOptionId);

    // console.log("222222--", variation);
    // console.log(variationDetails, "-------------------");

    // let fileUrl = req?.file.path.replace(/\\/g, "/").substring("".length);
    const info = {
        title: req.body.title,
        image: req?.file.path,
        categoryId: req.body.categoryId,
        price: req.body.price,
        stock: req.body.stock,
        shortDescription: req.body.shortDescription,
        longDescription: req.body.longDescription,
    };

    const product = await Product.create(info);

    //for product variation
    const allVariation = [];

    variation.map((i) => {
        // console.log("mq1", i);
        allVariation.push({ variationId: i, productId: product.id });
    });

    // console.log("variation", allVariation);

    const productVariation = await ProductVariation.bulkCreate(allVariation);
    // console.log("product---", productVariation);

    // product config

    const allData = [];

    variationDetails.map((i) => {
        // console.log("mq2", i);
        allData.push({ variationOptionId: i, productId: product.id });
    });

    // console.log("all", allData);

    const productConfig = await ProductConfig.bulkCreate(allData);

    // console.log("product---", productConfig);

    return res.status(201).json({ data: product });
};

const updateProduct = async (req, res) => {
    let id = req.params.id;

    const {
        title,
        catagary,
        price,
        stock,
        color,
        size,
        shortDescription,
        longDescription,
    } = req.body;

    const update = {
        title: title,
        catagary: catagary,
        price: price,
        stock: stock,
        color: color,
        size: size,
        shortDescription: shortDescription,
        image: req.body.path,
        longDescription: longDescription,
    };

    try {
        // console.log("This is Something", id);
        await Product.update(update, { where: { id: id } });
        return res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
};

const deleteProduct = async (req, res) => {
    let id = req.params.id;
    const {
        title,
        catagary,
        price,
        stock,
        color,
        size,
        shortDescription,
        longDescription,
    } = req.body;

    const update = {
        title: title,
        catagary: catagary,
        price: price,
        stock: stock,
        color: color,
        size: size,
        shortDescription: shortDescription,
        image: req.body.path,
        longDescription: longDescription,
    };

    try {
        await Product.update(update, { where: { id: id, deletedby: null } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
    // await Role.destroy({where:{id:id,deletedby:null}})
    // res.status(200).send('DELETED')
};

//for product get by id

const getOneProduct = async (req, res) => {
    try {
        let id = req.params.id;
        var oneProduct = await Product.findOne({
            where: { id: id },
            include: [
                { model: Category, attributes: ["id", "name"] },
                {
                    model: ProductVariation,
                    attributes: ["variationId"],
                    include: [
                        {
                            model: Variation,
                            attributes: ["id", "name"],
                        },
                    ],
                },
                {
                    model: ProductConfig,
                    attributes: ["variationOptionId"],
                    include: [
                        {
                            model: VariationOption,
                            attributes: ["id", "name", "variationId"],
                        },
                    ],
                },
            ],
        });
        const newArray = [];
        const addArray = [];

        oneProduct = JSON.parse(JSON.stringify(oneProduct));
        newArray.push(oneProduct);

        newArray.forEach((i) => {
            // console.log("==========", i);
            i.ProductConfigs.forEach((l) => {
                addArray.push(l.VariationOption);
            });
        });
        newArray.forEach((i) => {
            i.ProductVariations = JSON.parse(
                JSON.stringify(i.ProductVariations)
            );
            i.ProductVariations.forEach((l) => {
                l.variationOption = addArray.filter(
                    (j) => j.variationId == l.variationId
                );
            });
        });
        return res.status(200).send(oneProduct);
    } catch (err) {
        console.log("error", err);
    }
};

// for all product

const getAllProducts = async (req, res) => {
    try {
        var products = await Product.findAll({
            include: [
                {
                    model: Category,
                    attributes: ["id", "name"],
                },
                {
                    model: ProductVariation,
                    attributes: ["variationId"],
                    include: [
                        {
                            model: Variation,
                            attributes: ["id", "name"],
                        },
                    ],
                },
                {
                    model: ProductConfig,
                    attributes: ["variationOptionId"],
                    include: [
                        {
                            model: VariationOption,
                            attributes: ["id", "name", "variationId"],
                        },
                    ],
                },
            ],
        });
        const tempProduct = [];

        products = JSON.parse(JSON.stringify(products));
        products.forEach((i) => {
            i.ProductConfigs.forEach((l) => {
                tempProduct.push(l.VariationOption);

                jsonObject = tempProduct.map(JSON.stringify);

                uniqueSet = new Set(jsonObject);
                uniqueArray = Array.from(uniqueSet).map(JSON.parse);
            });
        });

        products.forEach((i) => {
            i.ProductVariations = JSON.parse(
                JSON.stringify(i.ProductVariations)
            );
            i.ProductVariations.forEach((l) => {
                l.variationOption = uniqueArray.filter(
                    (j) => j.variationId == l.variationId
                );
            });
        });
        return res.status(200).send(products);
    } catch (err) {
        console.log("Error", err);
    }
};

//product by category

const getProductByCategory = async (req, res) => {
    try {
        let id = req.params.id;
        var oneProduct = await Product.findAll({
            where: { categoryId: id },
            include: [
                { model: Category, attributes: ["id", "name"] },
                {
                    model: ProductVariation,
                    attributes: ["variationId"],
                    include: [
                        {
                            model: Variation,
                            attributes: ["id", "name"],
                        },
                    ],
                },
                {
                    model: ProductConfig,
                    attributes: ["variationOptionId"],
                    include: [
                        {
                            model: VariationOption,
                            attributes: ["id", "name", "variationId"],
                        },
                    ],
                },
            ],
        });
        const addArray = [];

        oneProduct.forEach((i) => {
            i.ProductConfigs.forEach((l) => {
                addArray.push(l.VariationOption);
            });
        });
        oneProduct.forEach((i) => {
            i.ProductVariations = JSON.parse(
                JSON.stringify(i.ProductVariations)
            );
            i.ProductVariations.forEach((l) => {
                l.variationOption = addArray.filter(
                    (j) => j.variationId == l.variationId
                );
            });
        });
        return res.status(200).send(oneProduct);
    } catch (err) {
        console.log("error", err);
    }
};

module.exports = {
    createProduct,
    updateProduct,
    deleteProduct,
    getOneProduct,
    getAllProducts,
    upload,
    getProductByCategory,
};
