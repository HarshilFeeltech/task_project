// const { auth } = require("../../middleware/auth");
const {
    createProduct,
    updateProduct,
    deleteProduct,
    getOneProduct,
    getAllProducts,
    getProductByCategory,
    upload,
} = require("./lib/controller");

module.exports = (app) => {
    app.get("/product/", getAllProducts);
    app.get("/product/:id", getOneProduct);
    app.get("/productCategory/:id", getProductByCategory);
    app.post("/product/", upload, createProduct);
    app.delete("/product/:id", deleteProduct);
    app.put("/product/:id", updateProduct);
};
