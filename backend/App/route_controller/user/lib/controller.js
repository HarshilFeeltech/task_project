const db = require("../../../db/models");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");

const User = db.User;

const signIn = (req, res) => {
    console.log("email", req.body.email);
    return User.findOne({
        where: {
            email: req.body.email,
        },
    }).then(async (result) => {
        // console.log("this is ", result?.id);
        if (!result) {
            return res.status(400).send({
                message: "User does not exist.",
            });
        }

        // if (!bcrypt.compareSync(req.body.password, result.password)) {
        //     return res.status(400).send({
        //         message: "invalid credentials.",
        //     });
        // }

        const payload = {
            user: {
                id: result.id,
                email: result.email,
                password: result.password,
            },
        };

        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }
            const token = jwt.sign(payload, process.env.JWT_SECRET_KEY);
            console.log(token);

            return res.send({
                message: "login success",
                accessToken: token,
            });
        } catch (err) {
            console.log(err?.message);
            return res.status(500).json({ message: "something went wrong" });
        }
    });
};

const addUser = async (req, res) => {
    try {
        const { userName, email, password } = req.body;
        const existingUser = await User.findOne({ where: { email } });
        if (existingUser) {
            return res.status(400).json({ message: "user already exists" });
        }

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const hashedPassword = await bcrypt.hash(password, 10);

        const result = await User.create({
            userName: userName,
            email: email,
            password: hashedPassword,
        });

        return res.status(201).json({ user: result });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "somthing went wrong" });
    }
};

const updateUser = async (req, res) => {
    const id = req.params.id;
    const { userName, email, password } = req.body;

    const hashedPassword = await bcrypt.hash(password, 10);

    const newUser = {
        userName: userName,
        email: email,
        password: hashedPassword,
        updatedBy: req.user.id,
    };
    await User.update(newUser, { where: { id: id } });
    return res.status(200).send(newUser);
};

const deleteUser = async (req, res) => {
    const currentDate = new Date();
    const timestamp = currentDate.getTime();
    let id = req.params.id;
    const { userName, email, password } = req.body;
    const update = {
        userName: userName,
        email: email,
        roleId: roleId,
        password: password,
        deletedAt: timestamp,
    };

    try {
        await User.update(update, { where: { id: id, deletedby: null } });
        return res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "please try again" });
    }
    // await User.destroy({where:{id:id}})
    // res.status(200).send('DELETED')
};
const getUsers = async (req, res) => {
    try {
        const users = await User.findAll({});
        return res.status(200).json(users);
        // const rolls = await Roll.findOne({userId : req.userId});
        // res.status(200).json(rolls);
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "something went wrong!" });
    }
};

const getUserById = async (req, res) => {
    try {
        const id = req.params.id;
        const user = await User.findOne({ where: { id: id } });
        return res.status(200).json(user);
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "something went wrong!" });
    }
};

module.exports = {
    addUser,
    signIn,
    updateUser,
    deleteUser,
    getUsers,
    getUserById,
};
