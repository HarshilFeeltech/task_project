// const express = require("express");
// const app = express.Router();
const { auth } = require("../../middleware/auth");
const {
    signIn,
    updateUser,
    deleteUser,
    getUsers,
    getUserById,
    addUser,
    //     upload,
} = require("../../route_controller/user/lib/controller");

const { validation, signInvalidation } = require("./lib/validation");

module.exports = (app) => {
    app.get("/user/allUser", auth, getUsers);
    app.get("/user/:id", auth, getUserById);
    app.post("/user/addUser", validation, addUser);
    app.post("/user/signIn", signInvalidation, signIn);
    app.put("/user/:id", auth, updateUser);
    app.delete("/user/:id", auth, deleteUser);
};
