// const express = require("express");
// const productConfigRouterr = express.Router();
// const { auth } = require("../../middleware/auth");
const {
    createProductConfig,
    updateProductConfig,
    deleteProductConfig,
    getOneProductConfig,
    getAllProductConfig,
} = require("../../route_controller/productConfig/lib/controller");

module.exports = (app) => {
    app.get("/productConfig/allConfig", getAllProductConfig);
    app.get("/productConfig/:id", getOneProductConfig);
    app.post("/productConfig/", createProductConfig);
    app.delete("/productConfig/:id", deleteProductConfig);
    app.put("/productConfig/:id", updateProductConfig);
};
