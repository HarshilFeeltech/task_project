const db = require("../../../db/models");

const VariationOption = db.VariationOption;
const Product = db.Product;
const Variation = db.Variation;
const ProductConfig = db.ProductConfig;

const createProductConfig = async (req, res) => {
    try {
        const { productId, variationOptionId } = req.body;

        const productConfig = await ProductConfig.create({
            productId: productId,
            variationOptionId: variationOptionId,
        });
        return res.status(201).json({ data: productConfig });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "something went wrong!" });
    }
};

const updateProductConfig = async (req, res) => {
    let id = req.params.id;

    const { productId, variationOptionId } = req.body;
    const update = {
        productId: productId,
        variationOptionId: variationOptionId,
    };

    try {
        await ProductConfig.update(update, { where: { id: id } });
        return res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
};

const deleteProductConfig = async (req, res) => {
    let id = req.params.id;
    const { productId, variationOptionId } = req.body;
    const update = {
        productId: productId,
        variationOptionId: variationOptionId,
    };

    try {
        await ProductConfig.update(update, { where: { id: id } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
    // await Role.destroy({where:{id:id,deletedby:null}})
    // res.status(200).send('DELETED')
};

const getOneProductConfig = async (req, res) => {
    try {
        let id = req.params.id;
        let oneVariationOption = await ProductConfig.findOne({
            where: { id: id },
            attributes: {
                exclude: [
                    "createdBy",
                    "updatedBy",
                    "deletedBy",
                    "updatedBy",
                    "createdAt",
                    "updatedAt",
                    "deletedAt",
                ],
            },
            include: [
                {
                    model: VariationOption,
                    attributes: {
                        exclude: [
                            "createdBy",
                            "updatedBy",
                            "deletedBy",
                            "updatedBy",
                            "createdAt",
                            "updatedAt",
                            "deletedAt",
                        ],
                    },
                },
            ],
        });
        const newArray = [];

        oneVariationOption = JSON.parse(JSON.stringify(oneVariationOption));
        newArray.push(oneVariationOption);

        console.log("----", oneVariationOption);

        return res.status(200).send(oneVariationOption);
    } catch (err) {
        console.log("error", err);
    }
};

const getAllProductConfig = async (req, res) => {
    const catagorys = await ProductConfig.findAll({});
    return res.status(200).send(catagorys);
};

module.exports = {
    createProductConfig,
    updateProductConfig,
    deleteProductConfig,
    getOneProductConfig,
    getAllProductConfig,
};
