module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.addColumn(
                    "role",
                    "createdBy",
                    {
                        type: Sequelize.DataTypes.UUID,
                        allowNull: true,
                        references: {
                            model: "userAdmin",
                            key: "id",
                        },
                        onDelete: "CASCADE",
                    },

                    { transaction: t }
                ),
                queryInterface.addColumn(
                    "role",
                    "updatedBy",
                    {
                        type: Sequelize.DataTypes.UUID,
                        allowNull: true,
                        references: {
                            model: "userAdmin",
                            key: "id",
                        },
                        onDelete: "CASCADE",
                    },
                    { transaction: t }
                ),
                queryInterface.addColumn(
                    "role",
                    "deletedBy",
                    {
                        type: Sequelize.DataTypes.UUID,
                        allowNull: true,
                        references: {
                            model: "userAdmin",
                            key: "id",
                        },
                        onDelete: "CASCADE",
                    },
                    { transaction: t }
                ),
            ]);
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.sequelize.transaction((t) => {
            return Promise.all([
                queryInterface.removeColumn("role", "createdBy", {
                    transaction: t,
                }),
                queryInterface.removeColumn("role", "updatedBy", {
                    transaction: t,
                }),
                queryInterface.removeColumn("role", "deletedBy", {
                    transaction: t,
                }),
            ]);
        });
    },
};
