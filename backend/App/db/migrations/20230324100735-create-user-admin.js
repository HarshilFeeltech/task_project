"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable("UserAdmin", {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                allowNull: false,
                primaryKey: true,
            },
            firstName: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            lastName: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            email: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            password: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            status: {
                type: Sequelize.ENUM("Active", "Inactive", "Pending"),
                defaultValue: "Active",
                allowNull: false,
            },
            contact: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            roleId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                    model: "role",
                    key: "id",
                },
                onDelete: "RESTRICT",
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            deletedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            createdBy: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                    model: "userAdmin",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
            updatedBy: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                    model: "userAdmin",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
            deletedBy: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                    model: "userAdmin",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable("UserAdmin");
    },
};
