module.exports = (sequelize, DataTypes) => {
    const Variation = sequelize.define(
        "Variation",
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            name: {
                type: DataTypes.STRING,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: "variation",
        }
    );

    Variation.associate = (models) => {
        Variation.belongsTo(models.UserAdmin, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "createdByUser",
        });
        Variation.belongsTo(models.UserAdmin, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "updatedByUser",
        });
        Variation.belongsTo(models.UserAdmin, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "deletedByUser",
        });

        Variation.hasMany(models.ProductVariation, {
            foreignKey: { name: "variationId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
    };
    return Variation;
};
