module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define(
        "Product",
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                primaryKey: true,
            },
            title: {
                type: DataTypes.STRING,
            },
            image: {
                type: DataTypes.STRING,
            },
            price: {
                type: DataTypes.STRING,
            },
            stock: {
                type: DataTypes.STRING,
            },
            shortDescription: {
                type: DataTypes.STRING,
            },
            longDescription: {
                type: DataTypes.STRING,
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE,
            },
            deletedAt: {
                allowNull: true,
                type: DataTypes.DATE,
            },
        },
        {
            tableName: "product",
        }
    );

    //relation

    Product.associate = (models) => {
        Product.belongsTo(models.Category, {
            foreignKey: { name: "categoryId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });

        Product.belongsTo(models.UserAdmin, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "createdByUser",
        });
        Product.belongsTo(models.UserAdmin, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "updatedByUser",
        });
        Product.belongsTo(models.UserAdmin, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "deletedByUser",
        });

        Product.hasMany(models.ProductConfig, {
            foreignKey: { name: "productId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });

        Product.hasMany(models.ProductVariation, {
            foreignKey: { name: "productId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
    };
    return Product;
};
