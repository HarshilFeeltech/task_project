module.exports = (sequelize, DataTypes) => {
    const Checkout = sequelize.define(
        "Checkout",
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                primaryKey: true,
            },
            firstName: {
                type: DataTypes.STRING,
            },
            lastName: {
                type: DataTypes.STRING,
            },
            country: {
                type: DataTypes.STRING,
            },
            streetAddress: {
                type: DataTypes.STRING,
            },
            town: {
                type: DataTypes.STRING,
            },
            state: {
                type: DataTypes.STRING,
            },
            postcode: {
                type: DataTypes.INTEGER,
            },
            phone: {
                type: DataTypes.INTEGER,
            },
            email: {
                type: DataTypes.STRING,
            },
            orderNote: {
                type: DataTypes.STRING,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
            deletedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: "checkout",
        }
    );

    //relation

    Checkout.associate = (models) => {
        Checkout.belongsTo(models.User, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "createdByUser",
        });
    };
    return Checkout;
};
