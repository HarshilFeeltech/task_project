module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define(
        "Role",
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            description: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            status: {
                type: DataTypes.ENUM("Active", "Inactive", "Pending"),
                defaultValue: "Active",
                allowNull: false,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
            deletedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: "role",
        }
    );

    //role relation
    Role.associate = (models) => {
        Role.belongsTo(models.UserAdmin, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "createdByUser",
        });
        Role.belongsTo(models.UserAdmin, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "updatedByUser",
        });
        Role.belongsTo(models.UserAdmin, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "deletedByUser",
        });

        //user relation
        Role.hasMany(models.UserAdmin, {
            foreignKey: { name: "roleId", allowNull: false },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
    };

    return Role;
};
