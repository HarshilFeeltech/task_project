module.exports = (sequelize, DataTypes) => {
    const UserAdmin = sequelize.define(
        "UserAdmin",
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                primaryKey: true,
            },
            firstName: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            lastName: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            password: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            status: {
                type: DataTypes.ENUM("Active", "Inactive", "Pending"),
                defaultValue: "Active",
                allowNull: false,
            },
            contact: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
            deletedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: "userAdmin",
        }
    );

    //self relations
    UserAdmin.associate = (models) => {
        UserAdmin.belongsTo(models.Role, {
            foreignKey: { name: "roleId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });

        //self
        UserAdmin.belongsTo(UserAdmin, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "createdByUserAdmin",
        });
        UserAdmin.belongsTo(UserAdmin, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "updatedByUserAdmin",
        });
        UserAdmin.belongsTo(UserAdmin, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "deletedByUserAdmin",
        });

        //has

        //role
        UserAdmin.hasMany(models.Role, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.Role, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.Role, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });

        //Category
        UserAdmin.hasMany(models.Category, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.Category, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.Category, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });

        //Product
        UserAdmin.hasMany(models.Product, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.Product, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.Product, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });

        //ProductConfig
        UserAdmin.hasMany(models.ProductConfig, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.ProductConfig, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.ProductConfig, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });

        //Variation
        UserAdmin.hasMany(models.Variation, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.Variation, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.Variation, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });

        //VariationOption
        UserAdmin.hasMany(models.VariationOption, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.VariationOption, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        UserAdmin.hasMany(models.VariationOption, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
    };
    return UserAdmin;
};
