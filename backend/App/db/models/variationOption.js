module.exports = (sequelize, DataTypes) => {
    const VariationOption = sequelize.define(
        "VariationOption",
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            name: {
                type: DataTypes.STRING,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: "variationOption",
        }
    );
    VariationOption.associate = (models) => {
        VariationOption.belongsTo(models.Variation, {
            foreignKey: { name: "variationId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });

        VariationOption.belongsTo(models.UserAdmin, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "createdByUser",
        });
        VariationOption.belongsTo(models.UserAdmin, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "updatedByUser",
        });
        VariationOption.belongsTo(models.UserAdmin, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "deletedByUser",
        });

        VariationOption.hasMany(models.ProductConfig, {
            foreignKey: { name: "variationOptionId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
    };
    return VariationOption;
};
