module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define(
        "User",
        {
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                primaryKey: true,
            },
            userName: {
                type: DataTypes.STRING,
            },
            password: {
                type: DataTypes.INTEGER,
            },
            email: {
                type: DataTypes.STRING,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
            deletedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: "user",
        }
    );

    //self relations
    User.associate = (models) => {
        User.belongsTo(User, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "createdByUser",
        });
        User.belongsTo(User, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "updatedByUser",
        });
        User.belongsTo(User, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "deletedByUser",
        });

        //has many relation

        User.hasMany(models.Checkout, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
    };
    return User;
};
