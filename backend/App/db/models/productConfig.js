module.exports = (sequelize, DataTypes) => {
    const ProductConfig = sequelize.define(
        "ProductConfig",
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                allowNull: false,
                primaryKey: true,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: "productConfig",
        }
    );
    ProductConfig.associate = (models) => {
        ProductConfig.belongsTo(models.Product, {
            foreignKey: { name: "productId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        ProductConfig.belongsTo(models.VariationOption, {
            foreignKey: { name: "variationOptionId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });

        ProductConfig.belongsTo(models.UserAdmin, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "createdByUser",
        });
        ProductConfig.belongsTo(models.UserAdmin, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "updatedByUser",
        });
        ProductConfig.belongsTo(models.UserAdmin, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "deletedByUser",
        });
    };
    return ProductConfig;
};
