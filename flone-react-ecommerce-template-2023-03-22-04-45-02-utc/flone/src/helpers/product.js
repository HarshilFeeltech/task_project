// get products
export const getProducts = (product, Category) => {
    const finalProducts = Category;
    // product.filter(
    //     (product) => product.Category.filter((single) => single === category)[0]
    // );
    // : product;
    return finalProducts;
};

// get product discount price
export const getDiscountPrice = (price, discount) => {
    return discount && discount > 0 ? price - price * (discount / 100) : null;
};

// get product cart quantity
export const getProductCartQuantity = (cartItems, product, color, size) => {
    let productInCart = cartItems.find(
        (single) =>
            single.id === product.id &&
            (single.selectedProductColor
                ? single.selectedProductColor === color
                : true) &&
            (single.selectedProductSize
                ? single.selectedProductSize === size
                : true)
    );
    if (cartItems.length >= 1 && productInCart) {
        if (product.variation) {
            return cartItems.find(
                (single) =>
                    single.id === product.id &&
                    single.selectedProductColor === color &&
                    single.selectedProductSize === size
            ).quantity;
        } else {
            return cartItems.find((single) => product.id === single.id)
                .quantity;
        }
    } else {
        return 0;
    }
};

export const cartItemStock = (item, color, size) => {
    if (item.stock) {
        return item.stock;
    } else {
        return item.variation
            .filter((single) => single.color === color)[0]
            .size.filter((single) => single.name === size)[0].stock;
    }
};

//get products based on category
export const getSortedProducts = (products, sortType, sortValue) => {
    if (products) {
        if (sortType === "category") {
            return products.filter(
                (Products) =>
                    products.category.filter(
                        (single) => single === sortValue
                    )[0]
            );
        }
        if (sortType === "tag") {
            return products.filter(
                (products) =>
                    products.tag.filter((single) => single === sortValue)[0]
            );
        }
        if (sortType === "color") {
            return products.filter(
                (product) =>
                    product.variation &&
                    product.variation.filter(
                        (single) => single.color === sortValue
                    )[0]
            );
        }
        if (sortType === "size") {
            return products.filter(
                (product) =>
                    product.variation &&
                    product.variation.filter(
                        (single) =>
                            single.size.filter(
                                (single) => single.name === sortValue
                            )[0]
                    )[0]
            );
        }
        if (sortType === "filterSort") {
            let sortProducts = [...products];
            if (sortValue === "default") {
                return sortProducts;
            }
            if (sortValue === "priceHighToLow") {
                return sortProducts.sort((a, b) => {
                    return b.price - a.price;
                });
            }
            if (sortValue === "priceLowToHigh") {
                return sortProducts.sort((a, b) => {
                    return a.price - b.price;
                });
            }
        }
    }
    return products;
};

// get individual element
const getIndividualItemArray = (array) => {
    let individualItemArray = array.filter(function (v, i, self) {
        return i === self.indexOf(v);
    });
    return individualItemArray;
};

// get individual categories
export const getIndividualCategories = (products) => {
    let productCategories = [];
    products?.map((product) => {
        return product.category.map((single) => {
            return productCategories.push(single);
        });
    });
    const individualProductCategories =
        getIndividualItemArray(productCategories);
    return individualProductCategories;
};

export const setActiveSort = (e) => {
    const filterButtons = document.querySelectorAll(
        ".sidebar-widget-list-left button, .sidebar-widget-tag button, .product-filter button"
    );
    filterButtons.forEach((item) => {
        item.classList.remove("active");
    });
    e.currentTarget.classList.add("active");
};

export const setActiveLayout = (e) => {
    const gridSwitchBtn = document.querySelectorAll(".shop-tab button");
    gridSwitchBtn.forEach((item) => {
        item.classList.remove("active");
        console.log("----------", gridSwitchBtn);
    });
    e.currentTarget.classList.add("active");
};

export const toggleShopTopFilter = (e) => {
    const shopTopFilterWrapper = document.querySelector(
        "#product-filter-wrapper"
    );
    shopTopFilterWrapper.classList.toggle("active");
    if (shopTopFilterWrapper.style.height) {
        shopTopFilterWrapper.style.height = null;
    } else {
        shopTopFilterWrapper.style.height =
            shopTopFilterWrapper.scrollHeight + "px";
    }
    e.currentTarget.classList.toggle("active");
};
