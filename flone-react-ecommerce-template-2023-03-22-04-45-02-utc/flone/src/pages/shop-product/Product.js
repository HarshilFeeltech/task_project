/* eslint-disable react-hooks/exhaustive-deps */
import React, { Fragment, useEffect, useState } from "react";
// import { useSelector } from "react-redux";
import { useParams, useLocation } from "react-router-dom";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import ProductDescriptionTab from "../../wrappers/product/ProductDescriptionTab";
import ProductImageDescription from "../../wrappers/product/ProductImageDescription";
import axios from "../../service/axios";
import service from "../../service/constant";
// import RelatedProductSlider from "../../wrappers/product/RelatedProductSlider";

const Product = () => {
    let { pathname } = useLocation();
    let { id } = useParams();
    const [data, setData] = useState([]);
    const getProduct = async (data) => {
        await axios
            .get("product/" + id, data)
            .then((res) => {
                setData(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };
    useEffect(() => {
        getProduct();
    }, []);
    return (
        <Fragment>
            <SEO
                titleTemplate="Product Page"
                description="Product Page of flone react minimalist eCommerce template."
            />

            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: "Home", path: process.env.PUBLIC_URL + "/" },
                        {
                            label: "Shop Product",
                            path: service.API_URL + pathname,
                        },
                    ]}
                />

                {/* product description with image */}
                <div>
                    <>
                        <ProductImageDescription
                            spaceTopClass="pt-100"
                            spaceBottomClass="pb-100"
                            product={data}
                        />
                        {/* product description tab */}
                        <ProductDescriptionTab
                            spaceBottomClass="pb-90"
                            productFullDesc={data.fullDescription}
                        />
                        {/* <RelatedProductSlider
                            spaceBottomClass="pb-95"
                            category={data.Category}
                        /> */}
                    </>
                </div>
            </LayoutOne>
        </Fragment>
    );
};

export default Product;
