/* eslint-disable react-hooks/exhaustive-deps */
import { Fragment, useEffect, useState } from "react";
import axios from "../../service/axios";
// import { useSelector } from "react-redux";
import { useLocation, useParams } from "react-router-dom";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import ProductDescriptionTab from "../../wrappers/product/ProductDescriptionTab";
import ProductImageDescription from "../../wrappers/product/ProductImageDescription";
import service from "../../service/constant";

const ProductTabLeft = () => {
    let { pathname } = useLocation();
    // const { products } = useSelector((state) => state.product);
    // const product = products.find(product => product.id === id);

    const { id } = useParams();
    const [product, setProduct] = useState([]);
    const getProduct = async (data) => {
        await axios
            .get("product/" + id, data)
            .then((res) => {
                setProduct(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };
    useEffect(() => {
        getProduct();
    }, []);
    return (
        <Fragment>
            <SEO
                titleTemplate="Product Page"
                description="Product page of flone react minimalist eCommerce template."
            />

            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: "Home", path: process.env.PUBLIC_URL + "/" },
                        {
                            label: "Shop Product",
                            path: service.API_URL + pathname,
                        },
                    ]}
                />
                <div>
                    {/* {product.map((item) => { */}
                    {/* return ( */}
                    <>
                        {/* product description with image */}
                        <ProductImageDescription
                            spaceTopClass="pt-100"
                            spaceBottomClass="pb-100"
                            product={product}
                            galleryType="leftThumb"
                        />
                        {/* product description tab */}
                        <ProductDescriptionTab
                            spaceBottomClass="pb-90"
                            productFullDesc={product.fullDescription}
                        />
                    </>
                </div>
            </LayoutOne>
        </Fragment>
    );
};

export default ProductTabLeft;
