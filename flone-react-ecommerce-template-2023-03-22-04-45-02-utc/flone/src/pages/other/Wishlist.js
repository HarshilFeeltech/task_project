import { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import { getDiscountPrice } from "../../helpers/product";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import {
    deleteFromWishlist,
    deleteAllFromWishlist,
} from "../../store/slices/wishlist-slice";
import service from "../../service/constant";

const Wishlist = () => {
    const dispatch = useDispatch();
    let { pathname } = useLocation();

    const newColor = [];
    const newSize = [];

    const currency = useSelector((state) => state.currency);
    const { wishlistItems } = useSelector((state) => state.wishlist);
    const { cartItems } = useSelector((state) => state.cart);

    // console.log("==========", wishlistItems);
    wishlistItems?.ProductVariations?.forEach((c) => {
        const newItem = c.variationOption.filter(
            (word) => !cartItems?.ProductVariations.includes(word)
        );

        if (newItem[0].variationId === 1) {
            newColor.push(newItem);
        } else {
            newSize.push(newItem);
        }
    });

    return (
        <Fragment>
            <SEO
                titleTemplate="Wishlist"
                description="Wishlist page of flone react minimalist eCommerce template."
            />
            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: "Home", path: process.env.PUBLIC_URL + "/" },
                        {
                            label: "Wishlist",
                            path: process.env.PUBLIC_URL + pathname,
                        },
                    ]}
                />
                <div className="cart-main-area pt-90 pb-100">
                    <div className="container">
                        {wishlistItems && wishlistItems.length >= 1 ? (
                            <Fragment>
                                <h3 className="cart-page-title">
                                    Your wishlist items
                                </h3>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="table-content table-responsive cart-table-content">
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Product Name</th>
                                                        <th>Unit Price</th>
                                                        <th>Add To Cart</th>
                                                        <th>action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {wishlistItems.map(
                                                        (wishlistItem, key) => {
                                                            console.log(
                                                                "------------",
                                                                wishlistItem
                                                            );
                                                            const discountedPrice =
                                                                getDiscountPrice(
                                                                    wishlistItem.price,
                                                                    wishlistItem.discount
                                                                );
                                                            const finalProductPrice =
                                                                (
                                                                    wishlistItem.price *
                                                                    currency.currencyRate
                                                                ).toFixed(2);
                                                            const finalDiscountedPrice =
                                                                (
                                                                    discountedPrice *
                                                                    currency.currencyRate
                                                                ).toFixed(2);

                                                            return (
                                                                <tr key={key}>
                                                                    <td className="product-thumbnail">
                                                                        <Link
                                                                            to={
                                                                                process
                                                                                    .env
                                                                                    .PUBLIC_URL +
                                                                                "/product/" +
                                                                                wishlistItem.id
                                                                            }
                                                                        >
                                                                            <img
                                                                                className="img-fluid"
                                                                                src={
                                                                                    service.API_URL +
                                                                                    wishlistItem.image
                                                                                }
                                                                                alt=""
                                                                            />
                                                                        </Link>
                                                                    </td>

                                                                    <td className="product-name text-center">
                                                                        <Link
                                                                            to={
                                                                                service.API_URL +
                                                                                "/product/" +
                                                                                wishlistItem.id
                                                                            }
                                                                        >
                                                                            {
                                                                                wishlistItem.title
                                                                            }
                                                                        </Link>
                                                                    </td>

                                                                    <td className="product-price-cart">
                                                                        {discountedPrice !==
                                                                        null ? (
                                                                            <Fragment>
                                                                                <span className="amount old">
                                                                                    {currency.currencySymbol +
                                                                                        finalProductPrice}
                                                                                </span>
                                                                                <span className="amount">
                                                                                    {currency.currencySymbol +
                                                                                        finalDiscountedPrice}
                                                                                </span>
                                                                            </Fragment>
                                                                        ) : (
                                                                            <span className="amount">
                                                                                {currency.currencySymbol +
                                                                                    finalProductPrice}
                                                                            </span>
                                                                        )}
                                                                    </td>

                                                                    <td className="product-wishlist-cart">
                                                                        <Link
                                                                            to={`${process.env.PUBLIC_URL}/product/${wishlistItem.id}`}
                                                                        >
                                                                            Select
                                                                            option
                                                                        </Link>
                                                                    </td>

                                                                    <td className="product-remove">
                                                                        <button
                                                                            onClick={() =>
                                                                                dispatch(
                                                                                    deleteFromWishlist(
                                                                                        wishlistItem.id
                                                                                    )
                                                                                )
                                                                            }
                                                                        >
                                                                            <i className="fa fa-times"></i>
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                            );
                                                        }
                                                    )}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="cart-shiping-update-wrapper">
                                            <div className="cart-shiping-update">
                                                <Link
                                                    to={
                                                        process.env.PUBLIC_URL +
                                                        "/shop-grid-standard"
                                                    }
                                                >
                                                    Continue Shopping
                                                </Link>
                                            </div>
                                            <div className="cart-clear">
                                                <button
                                                    onClick={() =>
                                                        dispatch(
                                                            deleteAllFromWishlist()
                                                        )
                                                    }
                                                >
                                                    Clear Wishlist
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Fragment>
                        ) : (
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="item-empty-area text-center">
                                        <div className="item-empty-area__icon mb-30">
                                            <i className="pe-7s-like"></i>
                                        </div>
                                        <div className="item-empty-area__text">
                                            No items found in wishlist <br />{" "}
                                            <Link
                                                to={
                                                    process.env.PUBLIC_URL +
                                                    "/shop-grid-standard"
                                                }
                                            >
                                                Add Items
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </LayoutOne>
        </Fragment>
    );
};

export default Wishlist;
