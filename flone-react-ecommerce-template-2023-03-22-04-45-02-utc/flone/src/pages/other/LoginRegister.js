import React, { Fragment, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import axios from "axios";
import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";

const LoginRegister = () => {
    let { pathname } = useLocation();

    const [email, setEmail] = useState("");
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");

    // const register = async (event) => {
    //     event.preventDefault();

    //     try {
    //         const response = await axios.post(
    //             "http://localhost:8001/user/addUser",
    //             {
    //                 userName,
    //                 password,
    //                 email,
    //             }
    //         );
    //         console.log(response.data);
    //     } catch (error) {
    //         console.log(error);
    //     }
    // };
    const register = (e) => {
        e.preventDefault();
        axios
            .post("http://localhost:8001/user/addUser", {
                email: email,
                userName: userName,
                password: password,
            })
            .then((response) => {
                if (response.data.message) {
                    console.log(response.data.message);
                } else {
                    console.log("account created successful");
                }
            });
    };
    const login = async (e) => {
        e.preventDefault();

        try {
            const response = await axios.post(
                "http://localhost:8001/user/signIn",
                {
                    email,
                    password,
                }
            );
            const token = response.data.accessToken;
            localStorage.setItem("token", token);
            return console.log({ message: "Token Seccessfull" });
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <Fragment>
            <SEO
                titleTemplate="Login"
                description="Login page of flone react minimalist eCommerce template."
            />
            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: "Home", path: process.env.PUBLIC_URL + "/" },
                        {
                            label: "Login Register",
                            path: process.env.PUBLIC_URL + pathname,
                        },
                    ]}
                />
                <div className="login-register-area pt-100 pb-100">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-7 col-md-12 ms-auto me-auto">
                                <div className="login-register-wrapper">
                                    <Tab.Container defaultActiveKey="login">
                                        <Nav
                                            variant="pills"
                                            className="login-register-tab-list"
                                        >
                                            <Nav.Item>
                                                <Nav.Link eventKey="login">
                                                    <h4>Login</h4>
                                                </Nav.Link>
                                            </Nav.Item>
                                            <Nav.Item>
                                                <Nav.Link eventKey="register">
                                                    <h4>Register</h4>
                                                </Nav.Link>
                                            </Nav.Item>
                                        </Nav>
                                        <Tab.Content>
                                            <Tab.Pane eventKey="login">
                                                <div className="login-form-container">
                                                    <div className="login-register-form">
                                                        <form onSubmit={login}>
                                                            <input
                                                                type="text"
                                                                name="user-email"
                                                                placeholder="Email"
                                                                onChange={(
                                                                    e
                                                                ) => {
                                                                    setEmail(
                                                                        e.target
                                                                            .value
                                                                    );
                                                                }}
                                                            />
                                                            <input
                                                                type="password"
                                                                name="user-password"
                                                                placeholder="Password"
                                                                onChange={(
                                                                    e
                                                                ) => {
                                                                    setPassword(
                                                                        e.target
                                                                            .value
                                                                    );
                                                                }}
                                                            />
                                                            <div className="button-box">
                                                                <div className="login-toggle-btn">
                                                                    <input type="checkbox" />
                                                                    <label className="ml-10">
                                                                        Remember
                                                                        me
                                                                    </label>
                                                                    <Link
                                                                        to={
                                                                            process
                                                                                .env
                                                                                .PUBLIC_URL +
                                                                            "/"
                                                                        }
                                                                    >
                                                                        Forgot
                                                                        Password?
                                                                    </Link>
                                                                </div>
                                                                <button type="submit">
                                                                    <span>
                                                                        Login
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </Tab.Pane>
                                            <Tab.Pane eventKey="register">
                                                <div className="login-form-container">
                                                    <div className="login-register-form">
                                                        <form>
                                                            <input
                                                                type="text"
                                                                name="user-name"
                                                                placeholder="username"
                                                                onChange={(
                                                                    e
                                                                ) => {
                                                                    setUserName(
                                                                        e.target
                                                                            .value
                                                                    );
                                                                }}
                                                            />
                                                            <input
                                                                type="password"
                                                                name="user-password"
                                                                placeholder="Password"
                                                                onChange={(
                                                                    e
                                                                ) => {
                                                                    setPassword(
                                                                        e.target
                                                                            .value
                                                                    );
                                                                }}
                                                            />
                                                            <input
                                                                name="user-email"
                                                                placeholder="Email"
                                                                type="email"
                                                                onChange={(
                                                                    e
                                                                ) => {
                                                                    setEmail(
                                                                        e.target
                                                                            .value
                                                                    );
                                                                }}
                                                            />
                                                            <div className="button-box">
                                                                <button
                                                                    type="submit"
                                                                    onClick={
                                                                        register
                                                                    }
                                                                >
                                                                    <span>
                                                                        Register
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </Tab.Pane>
                                        </Tab.Content>
                                    </Tab.Container>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </LayoutOne>
        </Fragment>
    );
};

export default LoginRegister;
