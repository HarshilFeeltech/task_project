/* eslint-disable react-hooks/exhaustive-deps */
import PropTypes from "prop-types";
import clsx from "clsx";
import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "../../service/axios";

const ProductDescriptionTab = ({ spaceBottomClass }) => {
    const { id } = useParams();
    const [data, setData] = useState([]);
    const getData = async (data) => {
        return await axios.get("product/" + id, data).then((res) => {
            setData(res?.data);
        });
    };
    useEffect(() => {
        getData();
    }, []);
    return (
        <div className={clsx("description-review-area", spaceBottomClass)}>
            <div className="container">
                <div className="description-review-wrapper">
                    <Tab.Container defaultActiveKey="productDescription">
                        <Nav
                            variant="pills"
                            className="description-review-topbar"
                        >
                            <Nav.Item>
                                <Nav.Link eventKey="productDescription">
                                    Description
                                </Nav.Link>
                            </Nav.Item>
                        </Nav>
                        <Tab.Content className="description-review-bottom">
                            <Tab.Pane eventKey="productDescription">
                                {data.longDescription}
                            </Tab.Pane>
                        </Tab.Content>
                    </Tab.Container>
                </div>
            </div>
        </div>
    );
};

ProductDescriptionTab.propTypes = {
    productFullDesc: PropTypes.string,
    spaceBottomClass: PropTypes.string,
};

export default ProductDescriptionTab;
