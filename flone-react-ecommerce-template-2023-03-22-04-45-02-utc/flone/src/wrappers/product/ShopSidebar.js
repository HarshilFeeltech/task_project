import PropTypes from "prop-types";
import clsx from "clsx";
import Aaa from "../../components/product/aaa";
import ShopSearch from "../../components/product/ShopSearch";
import { getIndividualCategories } from "../../helpers/product";
// import { useEffect, useState } from "react";
// import axios from "../../service/constant";

const ShopSidebar = ({ sideSpaceClass, products }) => {
    const uniqueCategories = getIndividualCategories(products);
    return (
        <div className={clsx("sidebar-style", sideSpaceClass)}>
            {/* shop search */}
            <ShopSearch />
            {/* filter by categories */}
            <Aaa categories={uniqueCategories} />
        </div>
    );
};

ShopSidebar.propTypes = {
    sideSpaceClass: PropTypes.string,
};

export default ShopSidebar;
