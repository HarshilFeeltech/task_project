/* eslint-disable no-unused-vars */
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

// axios import
import axios from "axios";

// service object import
import service from "./constant";

// auth config import
import authConfig from "../auth/config";

import { useDispatch } from "react-redux";

const instance = axios.create({
    baseURL: service.API_URL,
});

const AxiosInterceptor = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        // ** Request Interceptor
        const reqInterceptors = instance.interceptors.request.use(
            (config) => {
                // ** Get token from localStorage
                const accessToken = localStorage.getItem(
                    authConfig.storageTokenKeyName
                );

                // ** If token is present add it to request's Authorization Header
                if (accessToken) {
                    // ** eslint-disable-next-line no-param-reassign
                    config.headers.Authorization = JSON.parse(accessToken);
                }
                return config;
            },
            (error) => {
                return Promise.reject(error);
            }
        );

        // ** Add request/response interceptor
        const resInterceptors = instance.interceptors.response.use(
            (response) => {
                return response;
            },
            (error) => {
                // ** const { config, response: { status } } = error
                const { response } = error;

                // ** if (status === 404)
                if (response && response.status === 404) {
                    return Promise.reject(error);
                }

                return Promise.reject(error);
            }
        );
        return () => {
            instance.interceptors.request.eject(reqInterceptors);
            instance.interceptors.response.eject(resInterceptors);
        };
    }, []);

    return;
};

export default instance;

export { AxiosInterceptor };
