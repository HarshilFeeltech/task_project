/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import PropTypes from "prop-types";
import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getProductCartQuantity } from "../../helpers/product";
import Rating from "./sub-components/ProductRating";
import { addToCart } from "../../store/slices/cart-slice";
import { addToWishlist } from "../../store/slices/wishlist-slice";
import { addToCompare } from "../../store/slices/compare-slice";
import { render } from "react-dom";
// import axios from "../../service/axios";

const ProductDescriptionInfo = ({
    product,
    currency,
    cartItems,
    wishlistItem,
}) => {
    const dispatch = useDispatch();
    const newColor = [];
    const newSize = [];
    const newCategory = [];

    const [selectedProductColor, setSelectedProductColor] = useState(
        product.variation ? product.variation[0].color : ""
    );
    const [selectedProductSize, setSelectedProductSize] = useState(
        product.variation ? product.variation[0].size[0].name : ""
    );
    const [productStock, setProductStock] = useState(
        product.variation ? product.variation[0].size[0].stock : product.stock
    );
    const [quantityCount, setQuantityCount] = useState(1);
    const productCartQty = getProductCartQuantity(cartItems, product);

    //variation

    newCategory.push(product.Category);

    // console.log("---------", product.Category.name);
    product?.ProductVariations?.forEach((c) => {
        const newItem = c.variationOption.filter(
            (word) => !product?.ProductVariations.includes(word)
        );

        if (newItem[0].variationId === 1) {
            newColor.push(newItem);
        } else {
            newSize.push(newItem);
        }
    });

    // console.log("=======", product);
    return (
        <div className="product-details-content ml-70">
            <h2>{product.title}</h2>
            <div className="product-details-price">
                <Fragment>
                    <span>{currency.currencySymbol + product.price}</span>{" "}
                </Fragment>
            </div>
            <div className="pro-details-rating-wrap">
                <div className="pro-details-rating">
                    <Rating ratingValue={product.rating} />
                </div>
            </div>
            <div className="pro-details-list">
                <p>{product.shortDescription}</p>
            </div>

            <div className="pro-details-size-color">
                <div className="pro-details-size">
                    <span className="">Color</span>
                    <div className="pro-details-size-content">
                        {newColor.flat().map((single, key) => {
                            return (
                                <label
                                    className={`pro-details-size-content--single`}
                                    key={key}
                                >
                                    <input
                                        type="radio"
                                        value={single.id}
                                        checked={
                                            single.name === selectedProductColor
                                                ? "checked"
                                                : ""
                                        }
                                        onChange={() => {
                                            setSelectedProductColor(
                                                single.name
                                            );
                                            setQuantityCount(1);
                                        }}
                                    />
                                    <span className="size-name">
                                        {single.name}
                                    </span>
                                </label>
                            );
                        })}
                    </div>
                </div>
                <div className="pro-details-size">
                    <span>Size</span>
                    <div className="pro-details-size-content">
                        {newSize.flat().map((singleSize, key) => {
                            return (
                                <label
                                    className={`pro-details-size-content--single`}
                                    key={key}
                                >
                                    <input
                                        type="radio"
                                        value={singleSize.name}
                                        checked={
                                            singleSize.name ===
                                            selectedProductSize
                                                ? "checked"
                                                : ""
                                        }
                                        onChange={() => {
                                            setSelectedProductSize(
                                                singleSize.name
                                            );
                                            setProductStock(singleSize.stock);
                                            setQuantityCount(1);
                                        }}
                                    />
                                    <span className="size-name">
                                        {singleSize.name}
                                    </span>
                                </label>
                            );
                        })}
                    </div>
                </div>
            </div>

            {product.affiliateLink ? (
                <div className="pro-details-quality">
                    <div className="pro-details-cart btn-hover ml-0">
                        <a
                            href={product.affiliateLink}
                            rel="noopener noreferrer"
                            target="_blank"
                        >
                            Buy Now
                        </a>
                    </div>
                </div>
            ) : (
                <div className="pro-details-quality">
                    <div className="cart-plus-minus">
                        <button
                            onClick={() =>
                                setQuantityCount(
                                    quantityCount > 1 ? quantityCount - 1 : 1
                                )
                            }
                            className="dec qtybutton"
                        >
                            -
                        </button>
                        <input
                            className="cart-plus-minus-box"
                            type="text"
                            value={quantityCount}
                            readOnly
                        />
                        <button
                            onClick={() =>
                                setQuantityCount(
                                    quantityCount <
                                        product.stock - productCartQty
                                        ? quantityCount + 1
                                        : quantityCount
                                )
                            }
                            className="inc qtybutton"
                        >
                            +
                        </button>
                    </div>
                    <div className="pro-details-cart btn-hover">
                        {product.stock && product.stock > 0 ? (
                            <button
                                onClick={() =>
                                    dispatch(
                                        addToCart({
                                            ...product,
                                            quantity: quantityCount,
                                            selectedProductColor:
                                                selectedProductColor
                                                    ? selectedProductColor
                                                    : product.selectedProductColor
                                                    ? product.selectedProductColor
                                                    : null,
                                            selectedProductSize:
                                                selectedProductSize
                                                    ? selectedProductSize
                                                    : product.selectedProductSize
                                                    ? product.selectedProductSize
                                                    : null,
                                        })
                                    )
                                }
                                disabled={productCartQty >= productStock}
                            >
                                {" "}
                                Add To Cart{" "}
                            </button>
                        ) : (
                            <button disabled>Out of Stock</button>
                        )}
                    </div>
                    <div className="pro-details-wishlist">
                        <button
                            className={
                                wishlistItem !== undefined ? "active" : ""
                            }
                            disabled={wishlistItem !== undefined}
                            title={
                                wishlistItem !== undefined
                                    ? "Added to wishlist"
                                    : "Add to wishlist"
                            }
                            onClick={() => dispatch(addToWishlist(product))}
                        >
                            <i className="pe-7s-like" />
                        </button>
                    </div>
                </div>
            )}
            <div className="pro-details-meta">
                <span>Categories : </span>

                <ul>
                    {newCategory?.map((i) => {
                        return <li>{i?.name} </li>;
                    })}
                </ul>
            </div>

            <div className="pro-details-social">
                <ul>
                    <li>
                        <a href="//facebook.com">
                            <i className="fa fa-facebook" />
                        </a>
                    </li>
                    <li>
                        <a href="//dribbble.com">
                            <i className="fa fa-dribbble" />
                        </a>
                    </li>
                    <li>
                        <a href="//pinterest.com">
                            <i className="fa fa-pinterest-p" />
                        </a>
                    </li>
                    <li>
                        <a href="//twitter.com">
                            <i className="fa fa-twitter" />
                        </a>
                    </li>
                    <li>
                        <a href="//linkedin.com">
                            <i className="fa fa-linkedin" />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
};

ProductDescriptionInfo.propTypes = {
    cartItems: PropTypes.array,
    compareItem: PropTypes.shape({}),
    currency: PropTypes.shape({}),
    discountedPrice: PropTypes.number,
    finalDiscountedPrice: PropTypes.number,
    finalProductPrice: PropTypes.number,
    product: PropTypes.shape({}),
    wishlistItem: PropTypes.shape({}),
};

export default ProductDescriptionInfo;
