import PropTypes from "prop-types";

import { setActiveSort } from "../../helpers/product";

const ShopTopFilter = ({ products, getSortParams }) => {
    console.log("===========", products);

    return (
        <div className="product-filter-wrapper" id="product-filter-wrapper">
            <div className="product-filter-wrapper__inner">
                <div className="row">
                    {/* Product Filter */}
                    <div className="col-md-3 col-sm-6 col-xs-12 mb-30">
                        <div className="product-filter">
                            <h5>Categories</h5>
                            <ul>
                                {products.Category.map((category, key) => {
                                    return (
                                        <li key={key}>
                                            <button
                                                onClick={(e) => {
                                                    getSortParams(
                                                        "category",
                                                        category
                                                    );
                                                    setActiveSort(e);
                                                }}
                                            >
                                                {category.name}
                                            </button>
                                        </li>
                                    );
                                })}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

ShopTopFilter.propTypes = {
    getSortParams: PropTypes.func,
    products: PropTypes.array,
};

export default ShopTopFilter;
