import PropTypes from "prop-types";
import service from "../../service/constant";

const ProductImageFixed = ({ product }) => {
    return (
        <div className="product-large-image-wrapper">
            <div className="product-img-badges">
                {/* <span className="pink">-10%</span>{" "} */}
                <span className="purple">New</span>
            </div>

            <div className="product-fixed-image">
                {product.image ? (
                    <img
                        src={service.API_URL + product.image}
                        alt=""
                        className="img-fluid"
                    />
                ) : (
                    ""
                )}
            </div>
        </div>
    );
};

ProductImageFixed.propTypes = {
    product: PropTypes.shape({}),
};

export default ProductImageFixed;
