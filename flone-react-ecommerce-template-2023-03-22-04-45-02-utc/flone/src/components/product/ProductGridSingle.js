/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import { Link, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import Rating from "./sub-components/ProductRating";
import { getDiscountPrice } from "../../helpers/product";
import ProductModal from "./ProductModal";
import { addToCart } from "../../store/slices/cart-slice";
import { addToWishlist } from "../../store/slices/wishlist-slice";
import service from "../../service/constant";

const ProductGridSingle = ({
    product,
    currency,
    cartItem,
    wishlistItem,
    compareItem,
    spaceBottomClass,
}) => {
    const [modalShow, setModalShow] = useState(false);
    const dispatch = useDispatch();

    return (
        <Fragment>
            <div>
                <>
                    <div className={clsx("product-wrap", spaceBottomClass)}>
                        <div className="product-img">
                            <Link
                                to={
                                    process.env.PUBLIC_URL +
                                    "/product/" +
                                    product.id
                                }
                            >
                                <img
                                    className="default-img"
                                    src={service.API_URL + product?.image}
                                    alt="img"
                                />
                            </Link>
                            {product.discount || product.new ? (
                                <div className="product-img-badges">
                                    {product.discount ? (
                                        <span className="pink">
                                            -{product.discount}%
                                        </span>
                                    ) : (
                                        ""
                                    )}
                                    {product.new ? (
                                        <span className="purple">New</span>
                                    ) : (
                                        ""
                                    )}
                                </div>
                            ) : (
                                ""
                            )}

                            <div className="product-action">
                                <div className="pro-same-action pro-wishlist">
                                    <button
                                        className={
                                            wishlistItem !== undefined
                                                ? "active"
                                                : ""
                                        }
                                        disabled={wishlistItem !== undefined}
                                        title={
                                            wishlistItem !== undefined
                                                ? "Added to wishlist"
                                                : "Add to wishlist"
                                        }
                                        onClick={() =>
                                            dispatch(addToWishlist(product))
                                        }
                                    >
                                        <i className="pe-7s-like" />
                                    </button>
                                </div>
                                <div className="pro-same-action pro-cart">
                                    {product.affiliateLink ? (
                                        <a
                                            href={product.affiliateLink}
                                            rel="noopener noreferrer"
                                            target="_blank"
                                        >
                                            {" "}
                                            Buy now{" "}
                                        </a>
                                    ) : product.variation &&
                                      product.variation.length >= 1 ? (
                                        <Link
                                            to={`${process.env.PUBLIC_URL}/product/${product.id}`}
                                        >
                                            Select Option
                                        </Link>
                                    ) : product.stock && product.stock > 0 ? (
                                        <button
                                            onClick={() =>
                                                dispatch(addToCart(product))
                                            }
                                            className={
                                                cartItem !== undefined &&
                                                cartItem.stock > 0
                                                    ? "active"
                                                    : ""
                                            }
                                            disabled={
                                                cartItem !== undefined &&
                                                cartItem.stock > 0
                                            }
                                            title={
                                                cartItem !== undefined
                                                    ? "Added to cart"
                                                    : "Add to cart"
                                            }
                                        >
                                            {" "}
                                            <i className="pe-7s-cart"></i>{" "}
                                            {cartItem !== undefined &&
                                            cartItem.stock > 0
                                                ? "Added"
                                                : "Add to cart"}
                                        </button>
                                    ) : (
                                        <button disabled className="active">
                                            Out of Stock
                                        </button>
                                    )}
                                </div>
                                <div className="pro-same-action pro-quickview">
                                    <button
                                        title="Quick View"
                                        onClick={() => setModalShow(true)}
                                    >
                                        <i className="pe-7s-look" />
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="product-content text-center">
                            <h3>
                                <Link
                                    to={
                                        process.env.PUBLIC_URL +
                                        "/product/" +
                                        product.id
                                    }
                                >
                                    {product?.title}
                                </Link>
                            </h3>
                            {/* <div className="product-rating">
                                <p>Rating</p>
                                <Rating ratingValue={product.rating} />
                            </div> */}
                            <div className="product-price">
                                <Fragment>
                                    <span>
                                        {currency.currencySymbol +
                                            product.price}
                                    </span>
                                </Fragment>
                            </div>
                        </div>
                    </div>
                    {/* product modal */}
                    <ProductModal
                        show={modalShow}
                        onHide={() => setModalShow(false)}
                        product={product}
                        currency={currency}
                        wishlistItem={wishlistItem}
                    />
                </>
            </div>
        </Fragment>
    );
};

ProductGridSingle.propTypes = {
    cartItem: PropTypes.shape({}),
    compareItem: PropTypes.shape({}),
    wishlistItem: PropTypes.shape({}),
    currency: PropTypes.shape({}),
    product: PropTypes.shape({}),
    sliderClassName: PropTypes.string,
    spaceBottomClass: PropTypes.string,
};

export default ProductGridSingle;
