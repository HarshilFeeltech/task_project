import axios from "../service/constant";
import React, { useEffect, useState } from "react";

const Product = () => {
    const [item, setItem] = useState([]);
    // const { id } = useParams("");
    const getData = async (data) => {
        return await axios.get("product/", data).then((res) => {
            setItem(res.data);
        });
    };
    useEffect(() => {
        getData();
    }, []);

    return <div>product</div>;
};

export default Product;
