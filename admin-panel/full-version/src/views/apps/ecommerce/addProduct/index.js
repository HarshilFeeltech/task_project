// ** React Imports
import { Fragment } from 'react'
// import { useParams } from 'react-router-dom'

// ** Custom Components
import BreadCrumbs from '@components/breadcrumbs'

// ** Store & Actions
// import { useDispatch } from 'react-redux'
// import { getProduct } from '../store'

import '@styles/base/pages/app-ecommerce-details.scss'
import AddProduct from './AddProduct'

const Details = () => {
  // ** Vars
  //   const params = useParams().product
  //   const productId = params.substring(params.lastIndexOf('-') + 1)

  // ** Store Vars
  //   const dispatch = useDispatch()
  //   const store = useSelector(state => state.ecommerce)

  // ** ComponentDidMount : Get product
  //   useEffect(() => {
  //     dispatch(getProduct(productId))
  //   }, [])

  return (
    <Fragment>
      <BreadCrumbs title='Product Details' data={[{ title: 'eCommerce' }, { title: 'Add Product' }]} />
      <div className='app-ecommerce-details'>
        <AddProduct />
      </div>
    </Fragment>
  )
}

export default Details
