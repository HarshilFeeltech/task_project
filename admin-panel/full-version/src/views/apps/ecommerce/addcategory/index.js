// ** React Imports
import { Fragment } from 'react'
// import { useParams } from 'react-router-dom'

// ** Custom Components
import BreadCrumbs from '@components/breadcrumbs'

// ** Store & Actions
// import { useDispatch } from 'react-redux'
// import { getProduct } from '../store'

import '@styles/base/pages/app-ecommerce-details.scss'
import AddCategory from './AddCategory'

const Details = () => {
  // ** Vars
  //   const params = useParams().product
  //   const productId = params.substring(params.lastIndexOf('-') + 1)

  // ** Store Vars
  //   const dispatch = useDispatch()
  //   const store = useSelector(state => state.ecommerce)

  // ** ComponentDidMount : Get product
  //   useEffect(() => {
  //     dispatch(getProduct(productId))
  //   }, [])

  return (
    <Fragment>
      <BreadCrumbs title='Product Details' data={[{ title: 'eCommerce' }, { title: 'Add Category' }]} />
      <div className='app-ecommerce-details'>
        <AddCategory />
      </div>
    </Fragment>
  )
}

export default Details
