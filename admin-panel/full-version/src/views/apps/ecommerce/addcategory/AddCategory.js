// ** Reactstrap Imports
import axios from 'axios'
import { useState } from 'react'
import { Card, CardHeader, CardTitle, CardBody, Col, Input, Form, Button, Label, Row } from 'reactstrap'
import AddNewModal from '../../../tables/data-tables/basic/AddNewModal'

const AddCategory = () => {
  const [name, setName] = useState(' ')
  const [modal, setModal] = useState(false)
  const handleModal = () => setModal(!modal)
  const submit = async e => {
    e.preventDefault()
    try {
      const response = await axios.post('http://localhost:8001/category/', {
        name
      })
      console.log('Success', response.data)
    } catch (error) {
      console.log('error', error)
    }
  }

  const resetForm = () => {
    setName('')
  }

  return (
    <Card>
      <CardHeader>
        <CardTitle tag='h4'>Add Category</CardTitle>
      </CardHeader>

      <CardBody>
        <Form onSubmit={submit}>
          <Row className='mb-1'>
            <Label sm='3' for='name'>
              Category
            </Label>
            <Col sm='9'>
              <Input
                type='text'
                name='name'
                id='name'
                placeholder='category'
                onChange={e => {
                  setName(e.target.value)
                }}
              />
            </Col>
          </Row>

          <Row>
            <AddNewModal open={modal} handleModal={handleModal} />
            <Col className='d-flex' md={{ size: 9, offset: 3 }}>
              <Button className='me-1' color='primary' type='submit' onClick={submit}>
                Submit
              </Button>
              <Button outline color='secondary' type='reset' onClick={() => resetForm()}>
                Reset
              </Button>
            </Col>
          </Row>
        </Form>
      </CardBody>
    </Card>
  )
}
export default AddCategory
