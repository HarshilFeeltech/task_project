// ** Reactstrap Imports
import { Card, CardHeader, CardTitle, CardBody, CardText, Input, Label } from 'reactstrap'

const TextareaFloatingLabel = () => {
  return (
    <Card>
      <CardHeader>
        <CardTitle tag='h4'>Long Description</CardTitle>
      </CardHeader>

      <CardBody>
        <div className='form-floating'>
          <Input
            type='textarea'
            name='text'
            id='floating-textarea'
            placeholder='Floating Label'
            style={{ minHeight: '100px' }}
          />
          <Label className='form-label' for='floating-textarea'>
            Long Description
          </Label>
        </div>
      </CardBody>
    </Card>
  )
}
export default TextareaFloatingLabel
