const service = {}

if (process.env.NODE_ENV === 'production') {
  service.API_URL = 'http://localhost:8001'
} else {
  service.API_URL = 'http://localhost:8001'
}

export default service
