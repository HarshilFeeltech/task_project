// ** React Imports
import { useState, useEffect } from 'react'

// ** React Router Dom
import { useHistory, useParams } from 'react-router-dom'

// ** loader import
import showLoader from '@components/loader'

// ** Third Party Components
import * as yup from 'yup'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { toast } from 'react-toastify'
import Select from 'react-select'
import classnames from 'classnames'

// ** Utils
import { selectThemeColors } from '@utils'

// ** Reactstrap Imports
import {
  CardFooter,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Button,
  Form,
  Label,
  Input,
  FormFeedback,
  Row,
  Col
} from 'reactstrap'

// ** Axios
import axios from '../../../../service/axios'

// ** sample upload image
import SampleUpload from '../../../../assets/images/upload.png'
import service from '../../../../service/constant'

const FormValidation = ({}) => {
  // ** Submit Button Status
  const [buttonStatus, setButtonStatus] = useState(false)
  const history = useHistory()

  const validationScheme = yup.object().shape({
    name: yup.string().required('Name is required.'),
    categoryName: yup.object().nullable().required('Category is a required.'),
    description: yup.string().required('Description is required.')
  })

  // ** Hook Form
  const {
    control,
    reset,
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({ mode: 'onChange', resolver: yupResolver(validationScheme) })

  // ** Submit Function
  const onSubmit = rawData => {
    console.log('rawData-----', rawData)
    setButtonStatus(true)
    showLoader(true)
    const formData = new FormData()
    formData.append('name', rawData.name)
    formData.append('description', rawData.description)
    formData.append('categoryId', rawData.categoryName.value)
    formData.append('banner', rawData.banner[0])

    // ** create banner
    const createBanner = async data => {
      return await axios.post('/banner', data, {
        headers: {
          'Content-Type': 'application/json'
        }
      })
    }

    // ** update banner
    const updateBanner = async (id, data) => {
      return await axios.put('/banner/' + id, data, {
        headers: {
          'Content-Type': 'application/json'
        }
      })
    }

    window.location.href.endsWith('add')
      ? createBanner(formData)
          .then(res => {
            showLoader(false)
            toast.success('Banner added Successfully.')
            setButtonStatus(false)
            history.push('/master/banner')
          })
          .catch(err => {
            showLoader(false)
            setButtonStatus(false)
            toast.error(err.response?.data?.message || 'Something went wrong!')
          })
      : updateBanner(id, formData)
          .then(res => {
            showLoader(false)
            toast.success('Banner update Successfully.')
            setButtonStatus(false)
            history.push('/master/banner')
          })
          .catch(err => {
            showLoader(false)
            setButtonStatus(false)
            toast.error(err.response?.data?.message || 'Something went wrong!')
          })
  }

  // ** Register The Field
  const { ref: refName, ...restName } = register('name')
  const { ref: refDescription, ...restDescription } = register('description')
  const { ref: refBanner, ...restBanner } = register('banner')

  // ** get banner data by id
  const { id } = useParams()
  const [bannerData, setBannerData] = useState()
  const getBannerById = async (id, includes = 0) => {
    return await axios
      .get('/banner/' + id, {
        headers: {
          'Content-Type': 'application/json',
          includes: includes
        }
      })
      .then(res => {
        showLoader(false)
        setBannerData(res.data)
      })
      .catch(err => {
        showLoader(false)
        toast.error(err.response?.data?.message || 'Something went wrong!')
      })
  }

  // ** Get Category Data
  const [categoryData, setCategoryData] = useState([])
  const getCategoryData = async () => {
    return await axios
      .get('/course-category', {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(res => {
        let options = []
        res.data.map(row => {
          options.push({ value: row.id, label: row.name })
        })
        setCategoryData(options)
      })
      .catch(err => {
        toast.error(err.response?.data?.message || 'Something went wrong!')
      })
  }

  useEffect(() => {
    getCategoryData()
    if (!window.location.href.endsWith('add')) {
      getBannerById(id)
    }
  }, [])

  const [base64, setBase64] = useState(null)
  const allowedType = ['image/png', 'image/jpeg', 'image/jpg']

  const displayImage = e => {
    const file = e.target.files[0]
    if (!file) {
      return setBase64(null)
    }
    if (file.size > 5242880) {
      toast.error('File size exceeds maximum limit 5 MB.')
      e.target.value = ''
      setBase64(null)
      return
    }
    if (file && !allowedType.includes(file.type)) {
      toast.error('Only .jpg, .jpeg, .png image formats are allowed.')
      e.target.value = ''
      setBase64(null)
      return
    }
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
      setBase64(reader.result)
    }
    reader.onerror = function (error) {
      console.log('Error: ', error)
    }
  }

  useEffect(() => {
    if (!window.location.href.endsWith('add')) {
      reset({
        name: bannerData?.name,
        description: bannerData?.description,
        categoryName: bannerData ? { value: bannerData?.Category?.id, label: bannerData?.Category?.name } : ''
      })
      bannerData?.banner ? setBase64(service.API_URL + '/' + bannerData?.banner) : null
    }
  }, [bannerData])

  return (
    <Card>
      <CardHeader>
        <CardTitle tag='h4'>{window.location.href.endsWith('add') ? 'Add Banner' : 'Edit Banner'}</CardTitle>
      </CardHeader>

      <Form onSubmit={handleSubmit(onSubmit)}>
        <CardBody className='col-md-12 col-xs-12'>
          <Col md='12'>
            <img
              src={base64 ? base64 : SampleUpload}
              alt=''
              style={{
                maxWidth: '-webkit-fill-available',
                marginBottom: '20px',
                maxHeight: '200px'
              }}
            />
          </Col>
          <Row>
            <Col md='4' sm='12' className='mb-1'>
              <Label>Banner</Label>
              <Input
                id='banner'
                name='banner'
                type='file'
                invalid={errors.banner && true}
                innerRef={refBanner}
                {...restBanner}
                onChange={e => {
                  restBanner.onChange(e)
                  displayImage(e)
                }}
              />
              {errors && errors.banner && <FormFeedback>{errors.banner.message}</FormFeedback>}
            </Col>
            <Col md='3' sm='12' className='mb-1'>
              <Label>Name</Label>
              <Input
                id='name'
                name='name'
                type='text'
                placeholder='Name'
                invalid={errors.name && true}
                {...restName}
                innerRef={refName}
              />
              {errors && errors.name && <FormFeedback>{errors.name.message}</FormFeedback>}
            </Col>

            <Col md='3' sm='12' className='mb-1'>
              <Label>Category Name</Label>
              <Controller
                id='categoryName'
                control={control}
                name='categoryName'
                render={({ field }) => (
                  <Select
                    isClearable
                    classNamePrefix='select'
                    placeholder={'Select Category'}
                    options={categoryData}
                    theme={selectThemeColors}
                    className={classnames('react-select', {
                      'is-invalid':
                        errors.categoryName?.message ||
                        errors.categoryName?.label.message ||
                        errors.categoryName?.value.message
                    })}
                    {...field}
                  />
                )}
              />

              {errors && errors.categoryName && (
                <FormFeedback>
                  {errors.categoryName?.message ||
                    errors.categoryName?.label.message ||
                    errors.categoryName?.value.message}
                </FormFeedback>
              )}
            </Col>

            <Col md='10' sm='12' className='mb-1'>
              <Label>Description</Label>
              <Input
                type='textarea'
                name='description'
                id='description'
                placeholder='Description'
                invalid={errors.description && true}
                {...restDescription}
                innerRef={refDescription}
              />
              {errors && errors.description && <FormFeedback>{errors.description.message}</FormFeedback>}
            </Col>
          </Row>
        </CardBody>
        <CardFooter>
          <Button className='me-1 mb-1' color='primary' type='submit' disabled={buttonStatus}>
            Submit
          </Button>
          <Button
            className='me-1 mb-1'
            outline
            color='secondary'
            onClick={() => {
              reset()
              setBase64(null)
            }}
            disabled={buttonStatus}
          >
            Reset
          </Button>
          <Button
            outline
            disabled={buttonStatus}
            className='me-1 mb-1'
            color='secondary'
            type='submit'
            onClick={e => {
              e.preventDefault()
              history.push('/master/banner')
            }}
          >
            Cancel
          </Button>
        </CardFooter>
      </Form>
    </Card>
  )
}

export default FormValidation
